    .model tiny
    .stack 100h
    .data
        A dw 8, 13, 21, 34, 55, 89, 144, 233, 377, 610 
        B dw 10 dup(?)
    .code
start:
    mov ax, @data
    mov ds, ax
    mov es, ax
    lea si, A
    lea di, B
    mov cx, 10
    rep movsw
    
    
    lea si, B
    mov cx, 10
print_Arr:          ; dx = 0, ax = 0~ABC~
    mov dx, 0       ; (dx ax) = 0x0000 0x0~ABC~ ~ABC~ - hex representation of ABC
    mov ax, [si]    ; ax = ABC
    mov bx, 10      
    div bx          ; ax = AB, dx = C
    mov bx, dx      ; bl = C - remainder
    mov bh, 10
    div bh          ; al = A, ah = B
    mov bh, ah
    ; print ABC by algorythm:
    ; if A != 0: goto printA
    ; if B != 0: goto printB
    ; if C != 0: goto printC
    ; printA
    ; printB
    ; printC
    ; print _space_
    cmp al, 0
    jg printA
    cmp bh, 0
    jg printB
    jmp printC
printA:
    mov dl, al      ; al = A, bh = B, bl = C    
    add dl, '0'
    mov ah, 2
    int 21h
printB:
    mov dl, bh
    add dl, '0'
    mov ah, 2
    int 21h
printC:
    mov dl, bl
    add dl, '0'
    mov ah, 2
    int 21h
    ; print _space_
    mov dl, ' '
    int 21h
    add si, 2
    loop print_Arr
    
    mov ax, 4c00h
    int 21h
    end start