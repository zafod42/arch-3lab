    
    .model tiny
    .stack 100h
    .data
        A db 23, 43, 12, 43, 12, 15, 54, 27, 12, 39
        B db 10 dup(?)
    .code
start:
    mov ax, @data
    mov ds, ax
    mov es, ax
    
    lea si, A
    lea di, B
    mov cx, 10
    rep movsb
    
    lea si, A
    mov cx, 10 
print_Arr:          ; AB - number
    xor ax, ax
    mov bh, 10      ; div
    mov al, [si]    ; al = AB <=> ax = AB    
    div bh          ; al = A, ah = B
    mov bl, ah
printA:
    mov dl, al      ; 
    add dl, '0'
    mov ah, 2
    int 21h
printB:
    mov dl, bl
    add dl, '0'
    mov ah, 2
    int 21h
    mov dl, ' '
    int 21h
    inc si
    loop print_Arr
    
    mov ax, 4c00h
    int 21h
    end start