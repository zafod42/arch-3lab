    ;
    ;       BEAUTIFUL STABLE VERSION
    ;       Paul Kalabin 5130904/20003
    ;
    .model tiny
    .stack 100h
    .data
    crlf db 13,10,'$'
    A dw 10 dup(0) 
    buff_max_len db 4
    buff_len db ?
    buff db '$','$','$','$'
    wrongNumFormat db 'Wrong number format',13, 10, '$'
    .code
start:
    
    mov ax, @data
    mov ds, ax
    mov bx, 0
    mov ch, 10
    lea di, A  
    mov ch, 10
input_start:            ; general cycle
input_n:    
    mov ah, 0ah         ; input string that contain number
    lea dx, buff_max_len
    int 21h
    mov ah, 9           ; \n
    lea dx, crlf
    int 21h
    lea si, buff_len    ; find out how long number is
    mov cl, [si]
    lea si, buff        ; address of buffer
    mov dx, 0           ; clear everything that used
    mov ax, 0           ; because we need byte work
    mov bx, 0           ; words should be clear
    
mov_buff:               ; mov to Array cycle
    mov bl, [si]        ; mov digit to BL
    cmp bl, '0'         ; if code less than 0x30
    jl ErrMsg           ; jmp to Error Message
    cmp bl, '9'         ; else if code less than 0x39
    jg ErrMsg           ; jmp to Error Message
                        ; --- If Everything is correct - start writing
                        ;     to !number! to memory
    mov bp, 10          
    mul bp              ; word multiplication
    sub bl, '0'         ; DX:AX = AX * BP
    add ax, bx          ; AX contains full number
    inc si
    cmp cl, 0
    dec cl
    jne mov_buff
    mov [di], ax        ; write num to memory
    add di, 2           ; DI contains addres of array
    cmp ch, 0
    dec ch
    jne input_start
    
    ; print \n
    mov ah, 2
    mov dl, 13
    int 21h
    mov dl, 10
    int 21h
    
    
    lea si, A
    mov cx, 10
print_Arr:          ; dx = 0, ax = 0~ABC~
    mov dx, 0       ; (dx ax) = 0x0000 0x0~ABC~ 
                    ; where ~ABC~ - hex representation of ABC
    mov ax, [si]    ; ax = ABC
    mov bx, 10      
    div bx          ; ax = AB, dx = C
    mov bx, dx      ; bl = C - remainder
    mov bh, 10
    div bh          ; al = A, ah = B
    mov bh, ah
    ; print ABC by algorithm:
    ; if A != 0: goto printA
    ; if B != 0: goto printB
    ; if C != 0: goto printC
    ; printA
    ; printB
    ; printC
    ; print _space_
    cmp al, 0
    jg printA
    cmp bh, 0
    jg printB
    jmp printC
printA:
    mov dl, al      ; al = A, bh = B, bl = C    
    add dl, '0'
    mov ah, 2
    int 21h
printB:
    mov dl, bh
    add dl, '0'
    mov ah, 2
    int 21h
printC:
    mov dl, bl
    add dl, '0'
    mov ah, 2
    int 21h
    ; print _space_
    mov dl, ' '
    int 21h
    add si, 2
    loop print_Arr
    
    mov ax, 4c00h
    int 21h
    
ErrMsg:
    mov ah, 9
    lea dx, wrongNumFormat
    int 21h
    jmp input_start
    end start